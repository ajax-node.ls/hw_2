const urlIp = 'https://api.ipify.org/?format=json';
const urlDataUser = 'http://ip-api.com/json/';
const infoBox = document.querySelector('.info');
const button = document.querySelector('.button');


const fields = 'continent,country,regionName,city,district';

// let ip;

let getIP = async (urlIp) => {
    try {
        let response = await fetch(`${urlIp}`);
        return response.json();
        // .then(res =>
        //     res.json()
        // )
        //     .then(resp =>
        //     {
        //         ip = resp.ip;
        //     });
    } catch (e) {
        console.log(e);
    }

};


let getDataFromIp = async (urlDataUser) => {
    try {
        let userID = await getIP(urlIp);
        let response = await fetch(`${urlDataUser}${userID.ip}?fields=${fields}&lang=ru`);
        let resp = await response.json();
        console.log(resp);
        console.log(userID.ip);

        const container = document.createElement('div');
        let inner = `
    <h3>Ваш IP: ${userID.ip}</h3>
    <h4>Континент: ${resp.continent}</h4>
    <p>Страна: ${resp.country}</p>
    <p>Город: ${resp.city}</p>
    <p>Регион: ${resp.regionName}</p>
    <p>Район: ${resp.district}</p>
     `;
        container.innerHTML = inner;
        infoBox.appendChild(container);

    } catch (e) {
        console.log(e);
    }

};


button.addEventListener('click', () => getDataFromIp(`${urlDataUser}`));

